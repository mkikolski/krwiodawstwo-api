import flask
from flask import Flask
import requests
from pandas import DataFrame
from globals import *
import time

app = Flask(__name__)

LAST_UPDATED = 0
DATA = None

@app.route('/')
def title():
    return 'krwiodawstwo api endpoints:\n*  /blood-state/<city-name>/'


@app.route('/blood-state/<string:city>/')
def city_blood_data(city: str):  # put application's code here
    global DATA, LAST_UPDATED
    DATA, LAST_UPDATED = fetch_data(DATA, LAST_UPDATED)
    print(DATA[city])
    return flask.jsonify(**DATA[city])


def fetch_data(data, last_updated):
    if time.time() - LAST_UPDATED > 3600 or DATA is None:
        try:
            scraped_data = requests.get("https://krew.info/zapasy/index.html",
                                        headers={"Content-Type": "text/html;charset=utf-8"})
            response = scraped_data.content.decode("utf-8")
            table = response[response.find("<tbody>") + 8:response.find("</tbody>")]
            trs = table.replace("\t", "").replace("\n", "").split("<tr>")[1:]
            blood_data = {}

            for tr in trs:
                tds = tr.split("<td>")[1:]
                bt = str()
                bank = {}
                for i in range(len(tds)):
                    if i == 0:
                        bt = tds[i][tds[i].find("<h3>") + 4:tds[i].find("</h3>")]
                        if bt.startswith("0 Rh+"):
                            bt = "0 Rh+"
                        blood_data.update({bt: {}})
                    else:
                        bank.update({tds[i][tds[i].find('alt="') + 5:tds[i].find('" /></td>')]: IMG_DEFINITIONS.get(
                            tds[i][tds[i].find('src="img/') + 9: tds[i].find('" alt=')])})
                blood_data.update({bt: bank})
            return DataFrame(blood_data).transpose().to_dict(), time.time()
        except Exception:
            return data, last_updated

    else:
        return data, last_updated


if __name__ == '__main__':
    app.run()
